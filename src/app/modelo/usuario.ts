export class Usuario {
  constructor(
    public _id: string,
    public nombre: string,
    public apellido: string,
    public correo: string,
    public contrasena: string,
    public rol: string,
    public imagen: string
  ) { }
}
