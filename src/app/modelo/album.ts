export class Album{
    constructor(
        public titulo: string,
        public descripcion: string,
        public anio: Number,
        public imagen: string,
        public artista: string
    ){}
}
