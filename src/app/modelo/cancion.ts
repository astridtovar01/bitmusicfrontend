export class Cancion{
    constructor(
        public numero: string,
        public tituloCancion: string,
        public duracion: string,
        public urlCancion: string,
        public album: string
    ){}
}
