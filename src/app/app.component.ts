import { Component, OnInit } from '@angular/core';
import { GLOBAL } from './servicio/global';
import { Router } from '@angular/router';
import { Usuario } from './modelo/usuario';
import { UsuarioService } from './servicio/usuario.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{
  public titulo = 'BIT MUSIC';
  public usuario: Usuario;
  public usuarioRegistro: Usuario;
  public identidad;
  public token;
  public url: string;

  constructor(
    private usuarioService : UsuarioService,
    private router: Router
  ){
    this.usuario = new Usuario('', '', '', '', '', 'ROLE_USER', '');
    this.usuarioRegistro = new Usuario('', '', '', '', '', 'ROLE_USER', '');
    this.url = GLOBAL.url;
  }

  ngOnInit(){
    // var txt = this.usuarioService.iniciarSesion();
    // console.log(txt);

    this.identidad = this.usuarioService.obtenerIdentidad();
    this.token = this.usuarioService.obtenerToken();

    console.log(this.identidad);
    console.log(this.token);
  }

  loguear(){
    // console.log(this.usuario);
    this.usuarioService.iniciarSesion(this.usuario).subscribe(
      (response: any) => {
        let identidad = response.usuario;
        this.identidad = identidad;

        if(!this.identidad._id){
          alert("usuario no identificado")
        }else{
          localStorage.setItem('identidad', JSON.stringify(identidad));
          this.usuarioService.iniciarSesion(this.usuario, 'true').subscribe(
            (response: any) => {
              let token = response.token;
              this.token = token;

              if(this.token.length <=0){
                alert("token inexistente")
              }else{
                localStorage.setItem('token', token);
                console.log(token);
                console.log(identidad);
                this.usuario = new Usuario('', '', '', '', '', 'ROLE_USER', '');
                this.router.navigateByUrl('/mis-datos');
              }
              // console.log(response);
            },
            error => {
              var errorMensaje = <any>error;

              if(errorMensaje != null){
                console.log(error);
              }
            }
          );
        }
        // console.log(response);
      },
      error => {
        var errorMensaje = <any>error;

        if(errorMensaje != null){
          console.log(error);
        }
      }
    );
  }

  cerrarSesion(){
    localStorage.removeItem('identidad');
    localStorage.removeItem('token');
    localStorage.clear();

    this.identidad = null;
    this.token = null;
  }

  registrarUsuario(){
    console.log(this.usuarioRegistro);
    this.usuarioService.registro(this.usuarioRegistro).subscribe(
      (response: any) => {
        let usuario = response.usuario;
        this.usuarioRegistro = usuario;

        if(!this.usuarioRegistro._id){
          alert("Error al registrarse");
        }else{
          alert(`Registro exitoso!!, ingrese con ${this.usuarioRegistro.correo}`);
          this.usuarioRegistro = new Usuario('', '', '', '', '', 'ROLE_USER', '');
        }
      },
      error => {
        var errorMensaje = <any>error;

        if(errorMensaje != null){
          console.log(error);
        }
      }
    );
  }

}
