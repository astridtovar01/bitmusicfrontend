import { GLOBAL } from './global';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
// import { HttpClient } from '@angular/common/http';
// import { GLOBAL } from './global';

// export class UsuarioService {
//     public url: String;

//     constructor(private http : HttpClient){
//         this.url = GLOBAL.url;
//     }

//     iniciarSesion(){
//         return 'Funciona!'
//     }
// }



//import 'rxjs/add/operator/map';

@Injectable()
export class UsuarioService {
    public identidad;
    public token;
    public url: string;

    constructor(private http : HttpClient ){
        this.url = GLOBAL.url;
    }

    iniciarSesion(usuarioLogueado, getHash = null){
        // return 'Funciona!'
        if(getHash != null){
            usuarioLogueado.getHash = getHash;
        }

        let json = JSON.stringify(usuarioLogueado);
        let parametros = json;

        let headers = new HttpHeaders({'Content-Type': 'application/json'});

        return this.http.post(this.url+'login', parametros, {headers: headers}).pipe(map(res => res));
        // return this.http.post(this.url+'login', {usuarioLogueado});

    }

    obtenerIdentidad(){
        let identidad = JSON.parse(localStorage.getItem('identidad'));
        if(identidad != "undefined"){
            this.identidad = identidad;
        }else{
            this.identidad = null;
        }

        return this.identidad;
    }

    obtenerToken(){
        let token = localStorage.getItem('token');
        if(token != "undefined"){
            this.token = token;
        }else{
            this.token = null;
        }

        return this.token;
    }

    registro(usuarioNuevo){
        let json = JSON.stringify(usuarioNuevo);
        let parametros = json;

        let headers = new HttpHeaders({'Content-Type': 'application/json'});

        return this.http.post(this.url+'registro', parametros, {headers: headers}).pipe(map(res => res));
    }

    actualizar(usuarioEditado){
        let json = JSON.stringify(usuarioEditado);
        let parametros = json;

        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.obtenerToken()
        });

        return this.http.put(this.url+'actualizar-usuario/'+usuarioEditado._id, parametros, {headers: headers}).pipe(map(res => res));
    }
}
