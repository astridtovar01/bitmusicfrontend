import { Routes, RouterModule } from '@angular/router';
import { EditarUsuarioComponent } from './components/editar-usuario.component';
import { AppComponent } from './app.component'
import { NgModule } from '@angular/core';

const routes: Routes = [
    {path: '', component: AppComponent},
    {path: 'mis-datos', component: EditarUsuarioComponent},
    {path: '**', component: EditarUsuarioComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}