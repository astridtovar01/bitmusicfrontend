import { Component, OnInit } from '@angular/core';
import { GLOBAL } from '../servicio/global';
import { Usuario } from '../modelo/usuario';
import { UsuarioService } from '../servicio/usuario.service';



@Component({
    selector: 'editar-usuario',
    templateUrl: '../vista/editar-usuario.html'
})

export class EditarUsuarioComponent implements OnInit{
    public titulo: string;
    public usuario: Usuario;
    public identidad;
    public token;
    public archivosSubir: Array<File>;
    public url: string;

    constructor(
        private usuarioService : UsuarioService
      ){
        this.identidad = this.usuarioService.obtenerIdentidad();
        this.token = this.usuarioService.obtenerToken();
        this.titulo = 'Actualizar mis datos';
        this.usuario = this.identidad;
        this.url = GLOBAL.url;
      }

    ngOnInit(){
        console.log('FUnciona!!')
    }

    editarUsuario(){
        console.log(this.usuario);

        this.usuarioService.actualizar(this.usuario).subscribe(
            (response: any) => {
                //console.log(typeof(response));
                //console.log(response.user)
                if(!response.user){
                    alert('Usuario no actualizado')
                }else{
                    //this.usuario = response.user;
                    localStorage.setItem('identidad', JSON.stringify(this.usuario));
                    console.log(this.usuario.nombre)
                    document.getElementById('nombre-usuario').innerHTML = this.usuario.nombre;

                    if(!this.archivosSubir){

                    }else{
                        this.obtenerArchivo(this.url+'subir-imagen-usuario/'+this.usuario._id, [], this.archivosSubir)
                        .then((result: any)=>{
                            this.usuario.imagen = result.imagen;
                            localStorage.setItem('identidad', JSON.stringify(this.usuario));

                            let rutaImagen = this.url+'obtener-imagen-usuario/'+this.usuario.imagen;
                            document.getElementById('imgUsuario').setAttribute('src', rutaImagen)

                            console.log(this.usuario);
                        })
                    }

                    alert('Actualización exitosa!!');
                }
            },
            error => {
                var errorMensaje = <any>error;

                if(errorMensaje != null){
                console.log(error);
                }
            }
        );
    }

    subirArchivo(fileInput: any){
        this.archivosSubir = <Array<File>>fileInput.target.files;
    }

    obtenerArchivo(url: string, params: Array<string>, files: Array<File>){
        var token = this.token;

        return new Promise(function (resolve, reject){
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();

            for(var i = 0; i < files.length; i++){
                formData.append('imagen', files[i], files[i].name);
            }

            xhr.onreadystatechange = function(){
                if(xhr.readyState == 4){
                    if(xhr.status == 200){
                        resolve(JSON.parse(xhr.response));
                    }else{
                        reject(xhr.response);
                    }
                }
            }

            xhr.open('POST', url, true);
            xhr.setRequestHeader('Authorization', token);
            xhr.send(formData);

        });
    }
}
